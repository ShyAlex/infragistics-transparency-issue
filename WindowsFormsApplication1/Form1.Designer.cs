﻿namespace WindowsFormsApplication1
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            this.ultraButton1 = new Infragistics.Win.Misc.UltraButton();
            this.ultraPanel1 = new Infragistics.Win.Misc.UltraPanel();
            this.ultraButton2 = new Infragistics.Win.Misc.UltraButton();
            this.ultraPanel1.ClientArea.SuspendLayout();
            this.ultraPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // ultraButton1
            // 
            appearance1.BackColor = System.Drawing.Color.Lime;
            appearance1.BackColorDisabled = System.Drawing.Color.Transparent;
            this.ultraButton1.Appearance = appearance1;
            this.ultraButton1.Enabled = false;
            this.ultraButton1.Location = new System.Drawing.Point(3, 12);
            this.ultraButton1.Name = "ultraButton1";
            this.ultraButton1.Size = new System.Drawing.Size(254, 57);
            this.ultraButton1.TabIndex = 0;
            this.ultraButton1.Text = "Button 1";
            this.ultraButton1.UseOsThemes = Infragistics.Win.DefaultableBoolean.False;
            // 
            // ultraPanel1
            // 
            appearance2.BackColor = System.Drawing.Color.Transparent;
            this.ultraPanel1.Appearance = appearance2;
            // 
            // ultraPanel1.ClientArea
            // 
            this.ultraPanel1.ClientArea.Controls.Add(this.ultraButton2);
            this.ultraPanel1.ClientArea.Controls.Add(this.ultraButton1);
            this.ultraPanel1.Location = new System.Drawing.Point(12, 12);
            this.ultraPanel1.Name = "ultraPanel1";
            this.ultraPanel1.Size = new System.Drawing.Size(260, 237);
            this.ultraPanel1.TabIndex = 1;
            // 
            // ultraButton2
            // 
            this.ultraButton2.Location = new System.Drawing.Point(66, 111);
            this.ultraButton2.Name = "ultraButton2";
            this.ultraButton2.Size = new System.Drawing.Size(121, 39);
            this.ultraButton2.TabIndex = 1;
            this.ultraButton2.Text = "Toggle Button 1 Enabled";
            this.ultraButton2.Click += new System.EventHandler(this.ultraButton2_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 261);
            this.Controls.Add(this.ultraPanel1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ultraPanel1.ClientArea.ResumeLayout(false);
            this.ultraPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraButton ultraButton1;
        private Infragistics.Win.Misc.UltraPanel ultraPanel1;
        private Infragistics.Win.Misc.UltraButton ultraButton2;
    }
}

